from pprint import pprint
from allocator.resource_allocator import get_costs


instances = {
    "us-east": {
        "large": 0.12,
        "xlarge": 0.23,
        "2xlarge": 0.45,
        "4xlarge": 0.774,
        "8xlarge": 1.4,
        "10xlarge": 2.82
    },
    "us-west": {
        "large": 0.14,
        "2xlarge": 0.413,
        "4xlarge": 0.89,
        "8xlarge": 1.3,
        "10xlarge": 2.97
    },
}

tests = [
    [24, 135, None],  # n cpus for x hours
    [1, None, 1.4],  # y price for x hours
    [6, 180, 65],   # n cpus for x hours for y price
]

for test in tests:
    costs = get_costs(instances, hours=test[0], cpus=test[1], price=test[2])
    pprint(costs)
