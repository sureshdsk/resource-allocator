from collections import Counter
from pprint import pprint
cpu_cores = {
    'large': 1,
    'xlarge': 2,
    '2xlarge': 4,
    '4xlarge': 8,
    '8xlarge': 16,
    '10xlarge': 32,
}

def get_costs(instances, hours, cpus, price=None):
    output = {}
    max_budget = price if price and not cpus else cpus
    instance_hours = hours
    for region in instances.keys():
        instance_prices = instances[region].items()
        instance_types = instances[region].keys()

        if price and not cpus:
            price_per_xhours = [p[1] * instance_hours for p in instance_prices]
            cpu_cores_map = dict(zip(price_per_xhours, instance_types))
            available_items = price_per_xhours
        else:
            cpu_cores_map = dict((v, k) for k, v in cpu_cores.iteritems())
            available_items = [cpu_cores[instance_type] for instance_type in instance_types]
        all_combinations = get_combinations(max_budget, available_items, [])
        best_combination = get_optimal_soluntion(all_combinations)
        # pprint(best_combination)
        if best_combination:
            output[region] = {}
            best_combination_dict = Counter(best_combination)
            output[region] = dict()
            output[region]['region'] = region
            output[region]['servers'] = [(cpu_cores_map[k], v) for k, v in best_combination_dict.iteritems()]
            total_cost = best_combination if price and not cpus else available_items
            output[region]['total_cost'] = "${:.2f}".format(sum(total_cost))

    return output

def get_combinations(max_price, available_items, allocated_items):
    if sum(allocated_items) == max_price:
        yield allocated_items
    elif sum(allocated_items) > max_price:
        pass
    elif not available_items:
        pass
    else:
        for c in get_combinations(max_price, available_items[:], allocated_items + [available_items[0]]):
            yield c
        for c in get_combinations(max_price, available_items[1:], allocated_items):
            yield c

def get_optimal_soluntion(all_combinations):
    solutions = list(all_combinations)
    return min(solutions, key=len) if solutions else False
