from __future__ import absolute_import
import unittest
from allocator.resource_allocator import get_costs

class ResourceAllocatorTestCase(unittest.TestCase):
    def setUp(self):
        self.instances = {
            "us-east": {
                "large": 0.12,
                "xlarge": 0.23,
                "2xlarge": 0.45,
                "4xlarge": 0.774,
                "8xlarge": 1.4,
                "10xlarge": 2.82
            },
            "us-west": {
                "large": 0.14,
                "2xlarge": 0.413,
                "4xlarge": 0.89,
                "8xlarge": 1.3,
                "10xlarge": 2.97
            },
        }

    def tearDown(self):
        pass

    def runTest(self):
        output = get_costs(self.instances, 1, None, 2.8)
        expected_output = {
              'us-east': {'region': 'us-east',
                'servers': [('8xlarge', 2)],
                'total_cost': '$2.80'
              }
        }
        self.assertEqual(output,expected_output)

if __name__ == '__main__':
    unittest.main()